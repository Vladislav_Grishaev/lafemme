var swiper = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 1,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1
        },
        // when window width is >= 480px
        992: {
          slidesPerView: 2
        },
        // when window width is >= 640px
        1440: {
          slidesPerView: 3
        },
        1500: {
            slidesPerView: 4
          }
      }
  });

let valTextEvents = document.querySelectorAll('.js-val-text')

for (let index = 0; index < valTextEvents.length; index++) {
    const valTextEvent = valTextEvents[index];
    if (valTextEvent.innerHTML == '') {
        valTextEvent.classList.add('val-text')
    }
}

let nextSlide = document.querySelector('.swiper-button-next')
let prevtSlide = document.querySelector('.swiper-button-prev')

nextSlide.addEventListener('click', () => {
    prevtSlide.classList.add('active-arrow')
})